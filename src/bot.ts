import { Channel, Client, TextChannel } from 'discord.js';
import auth from './auth.json';
import * as commands from './commands';

const client = new Client();

// matches slash commands i.e. `/inspiration`
// const COMMAND_REGEX = /^\/(.+?)\b/;

let INSPIRO_TAG: RegExp;

// Event listener when a user connected to the server.
client.on('ready', () => {
  INSPIRO_TAG = new RegExp(`^\<\@\!?${client.user.id}\>`);

  console.log(`Logged in as ${client.user.tag}!`);
});

// Event listener when a user sends a message in the chat.
client.on('message', msg => {
  const userRef = msg.content.match(INSPIRO_TAG);

  // check to see if the message is directed at inspiro
  if (!userRef) return;

  const content = msg.content.replace(userRef[0], '').trim();

  const command = content.split(' ')[0].trim();
  const payload = content.substr(content.search(' ')).trim();

  console.log('content', content);
  console.log('command', command);
  console.log('payload', payload);

  if (command === 'help') {
    commands.help(payload, msg);
    return;
  } else if (command === 'inspire') {
    commands.inspire(payload, client.user.id, msg);
    return;
  } else if (
    [
      'whos inspired',
      'whos inspired?',
      "who's inspired",
      "who's inspired?",
      'who is inspired',
      'who is inspired?',
    ].includes(content)
  ) {
    commands.inspiration(payload, msg);
    return;
  }
});

// Initialize bot by connecting to the server
client.login(auth.token);
