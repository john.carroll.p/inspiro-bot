import { Message } from 'discord.js';
import { db, USER_REF } from './util';

export async function inspire(payload: string, clientId: string, msg: Message) {
  const strings = payload.split(' ');
  const user = strings[0].match(USER_REF);

  if (strings.length !== 1) {
    const error =
      strings.map(s => `"${s}"`).join(' and ') +
      `?? What is this! You can only give inspiration to one player at a time. Duh!`;

    return msg.reply(error);
  } else if (!user) {
    return msg.reply(`I only respond to inspiration requests that look like \`@username\`.`);
  } else if (msg.author.id === user[1]) {
    const error = `you're trying to give inspiration to *yourself*?! Fuck off. You just lost inspiration.`;

    db.push(`inspiration/${user[1]}`, false);

    return msg.reply(error);
  } else if (user[1] === clientId) {
    db.push(`inspiration/${msg.author.id}`, true);

    return msg.reply(`you want to give *me* inspiration? Now that's *inspired*!`);
  }

  let message = `<@${msg.author.id}> gave ${strings[0]} *inspiration*!`;

  if (Math.random() < 0.4) {
    if (Math.random() < 0.3) {
      message += ` ${pickRandom(NEGATIVE_FLAVOR)}`;
    } else {
      message += ` ${pickRandom(POSITIVE_FLAVOR)}`;
    }
  }

  db.push(`inspiration/${user[1]}`, true);

  return msg.channel.send(message);
}

const POSITIVE_FLAVOR = [
  'And it was *great* inspiration!',
  'And what great inspiration it was!',
  `The **best** inspiration!`,
  `And... Wait?! It's *Super* inspiration??`,
];

const NEGATIVE_FLAVOR = [
  `And... Wait! This inspiration is expired!`,
  `So what if he's just regifting inspiration he got earlier?`,
];

function pickRandom(arr: string[]) {
  return arr[Math.floor(Math.random() * arr.length) + 1];
}
