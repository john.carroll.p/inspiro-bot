import { Message } from 'discord.js';
import { db } from './util';

export async function inspiration(_: string, msg: Message) {
  const inspirationData = db.getData('inspiration');

  const inspirees = Object.entries(inspirationData)
    .filter(([, v]) => !!v)
    .map(([k]) => `<@${k}>`);

  let response = '';

  if (inspirees.length === 0) {
    response = 'no one is inspired  :cry:';
  } else if (inspirees.length === 1) {
    response = inspirees.join('') + ' is inspired!';
  } else if (inspirees.length < 3) {
    response = inspirees.join(' and ') + ' are inspired!';
  } else {
    const last = inspirees.pop();

    response = inspirees.join(', ') + ` and ${last} are inspired!`;
  }

  return msg.reply(response);
}
