import { Message } from 'discord.js';

export async function help(_payload: string, msg: Message) {
  const message = `help you say! I'm great at that!
  
  - I can *inspire* people! e.g. \`@inspiro inspire USER\`
  - I can show you who's *inspired*! e.g. \`@inspiro who's inspired?\`
  - I can... show you this help message!

I'm... ah, sure there are other things?`;

  return msg.reply(message);
}
