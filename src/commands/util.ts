import { JsonDB } from 'node-json-db';

export const db = new JsonDB(`${__dirname}/db.json`, true, true, '/');
export const USER_REF = /^\<\@\!?(\d+)\>/;
