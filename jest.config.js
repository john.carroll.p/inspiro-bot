module.exports = {
  testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
  moduleDirectories: ['node_modules'],
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  modulePathIgnorePatterns: ['tmp'],
  coveragePathIgnorePatterns: ['/node_modules/', '/tests/'],
  coverageThreshold: {
    global: {
      branches: 60,
      functions: 70,
      lines: 70,
      statements: 70,
    },
  },
  collectCoverage: true,
  verbose: true,
  testURL: 'http://localhost/',
  moduleNameMapper: {
    // more specific path maps need to come before less specific path maps
    // as `@rschedule/core` also matches `@rschedule/core/generators`
    '@local-tests/(.*)': '<rootDir>/../../tests/$1',
    '@local-tasks/(.*)': '<rootDir>/../../tasks/$1',
  },
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.jest.json',
    },
  },
  preset: 'ts-jest',
  testMatch: null,
};
